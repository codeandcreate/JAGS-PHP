<?php
/**
 * JAGS file handlers
 * ==================
 * 
 * Contains default file handler for dynamic php files and an example for dynamically adding
 * a footer and header to your static gemini files. 
 * 
 * Exception handling is done via the JetAnotherGeminiServer class.
 */
class jags_file_type_handlers 
{
	/**
	 * Standard php file handler, which includes the php file to run it.
	 */
	public static function text_x_php($JAGSRequest, $JAGSReturn): array
	{
		$JAGSReturn['meta'] = 'text/gemini'; // overwrite data type
		ob_start(); // turns on output buffering
		include $JAGSRequest['file_path']; // output goes only to buffer
		if (empty($JAGSReturn['content'])) { // check if the include filled the content already
			$JAGSReturn['content'] = ob_get_contents(); // stores buffer contents to the variable
		}
		$JAGSReturn['file_size'] = strlen($JAGSReturn['content']);
		ob_end_clean();

		return $JAGSReturn;
	}

	/**
	 * Adds a directory based header and footer to your gemini files.
	 * Deactivated on default (look at config.sample.php) 
	 */
	public static function text_gemini($JAGSRequest, $JAGSReturn): array
	{
		// check if there is a header:
		$_header_gmi = dirname($JAGSRequest['file_path']) . "/_includes/header.gemini";
		if (file_exists($_header_gmi)) {
			$JAGSReturn['content']		= file_get_contents($_header_gmi);
			$JAGSReturn['file_size']	= filesize($_header_gmi);
		}

		// get requested content
		$JAGSReturn['content']   .= file_get_contents($JAGSRequest['file_path']);
		$JAGSReturn['file_size'] += filesize($JAGSRequest['file_path']);
		
		// check if there is a footer:
		$_footer_gmi = dirname($JAGSRequest['file_path']) . "/_includes/footer.gemini";
		if (file_exists($_footer_gmi)) {
			$JAGSReturn['content']		.= file_get_contents($_footer_gmi);
			$JAGSReturn['file_size']	+= filesize($_footer_gmi);
		}

		return $JAGSReturn;
	}
}