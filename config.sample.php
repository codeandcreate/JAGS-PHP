<?php
/*
 * Set the hosts configuration with of your certificate file(s).
 * All other settings are optional.
 *
 * You can generate a certificate using:
 *
 *  openssl req -x509 -newkey rsa:4096 -keyout key.pem -out cert.pem -days 365 -nodes
 *
 * Then combine the key and certificate and copy them to the certs directory:
 *
 *  cp cert.pem yourdomain.com.pem
 *  cat key.pem >> yourdomain.com.pem
 *
 * Do not use a passphrase!
 */

$config = [
//	'hosts' => [
//		'localhost' => [																// domain name
//			'root'				=> "default",										// folder below 'work_dir'
//			'cert'				=> "certs/yourdomain.com.pem",	// your cert file
//			'cert_domain'	=> "localhost"									// domain name registered in the cert
//		],
//	],

// Additional ssl options - shouldn't be used for a public host atm.
//	'ssl_verify_peer'				=> false,
//	'ssl_capture_peer_cert'	=> false,

// IP address to listen to (leave commented out to listen on all interfaces)
//	'ip' => "127.0.0.1",

// Port to listen on (1965 is the default)
//	'port' => "1965",

// Default index file(s).  If a file isn't specified then the server will
// default to an index file (like index.html on a web server).
// Since it is an array, you can also add for example "index.php" to it.
//	'default_index_file' => ["index.gemini"],

// Logging, setting this to false will disable logging (default is on/true);
//	'logging' => true, 

// Each value will be separated with \t by default.
//	'log_sep' => "\t",

// By default old logs will be deleted by "log_cleanup.php" after 30 days
//	'log_delete_after' => "30days",

// 'work_dir' is the folder that contains config.php, server.php, logs/, libs/, ...
//	'work_dir' => "/var/gemini/",

// The log folder is always relative to 'work_dir'
//  'log_dir' => "logs",

// If set to false, the auto loader for other jags-classes in lib is deactivated.
// The auto loader is needed for file type handlers on default.
//  'enable_autoloader' => true,

// On default there is only one file type handler defined; for php files. 
// If you don't want dynamic content on your server, just set a empty array on this setting.
// The handler for text/gemini is just an example to extend the functionality of serving gemini files. Take a look at the jags_file_type_handlers class.
// You can also set handlers for other file types based on minetype.
//  'file_type_handlers' => [
//    'text/x-php' => [
//      'class_name'  => "jags_file_type_handlers",
//      'function'    => "text_x_php"
//    ],
//    'text/gemini' => [
//      'class_name'  => "jags_file_type_handlers",
//      'function'    => "text_gemini"
//    ],
//  ],
];